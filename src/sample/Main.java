package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.awt.*;

import static java.awt.Color.RED;
import static java.awt.Color.getColor;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Pane root = new Pane();
        primaryStage.setTitle("Valgusfoor");
        primaryStage.setScene(new Scene(root, 300, 600));
        primaryStage.show();

//        Circle ring = new Circle(35);
//        root.getChildren().add(ring);
//        ring.setCenterX(100);
//        ring.setCenterY(160);
//        ring.setFill(javafx.scene.paint.Color.BLUE);
//
//        Rectangle kast = new Rectangle(40,40,180,60);
//        kast.setFill(javafx.scene.paint.Color.BLUE);
//        root.getChildren().add(kast);
//        kast.setOnMouseClicked(event -> {
//            if (kast.getFill() == javafx.scene.paint.Color.BLUE)
//                kast.setFill(javafx.scene.paint.Color.RED);
//            else
//                kast.setFill(javafx.scene.paint.Color.BLUE);
//
//        });

        Circle ring1 = new Circle(30);
        root.getChildren().add(ring1);
        ring1.setCenterX(100);
        ring1.setCenterY(60);
        ring1.setStroke(javafx.scene.paint.Color.GRAY);
        ring1.setStrokeWidth(5);

        Circle ring2 = new Circle(30);
        root.getChildren().add(ring2);
        ring2.setCenterX(100);
        ring2.setCenterY(120);
        ring2.setStroke(javafx.scene.paint.Color.GRAY);
        ring2.setStrokeWidth(5);

        Circle ring3 = new Circle(30);
        root.getChildren().add(ring3);
        ring3.setCenterX(100);
        ring3.setCenterY(180);
        ring3.setStroke(javafx.scene.paint.Color.GRAY);
        ring3.setStrokeWidth(5);

        Line post = new Line(100, 217, 100, 500);
        root.getChildren().add(post);
        post.setStrokeWidth(10);
        post.setStroke(javafx.scene.paint.Color.GRAY);

        ring1.setOnMouseEntered(mouseEvent -> {
            ring1.setFill(javafx.scene.paint.Color.RED);
        });
        ring1.setOnMouseExited(mouseEvent -> {
            ring1.setFill(javafx.scene.paint.Color.BLACK);
            ring2.setFill(javafx.scene.paint.Color.BLACK);
            ring3.setFill(javafx.scene.paint.Color.BLACK);
        });

        ring2.setOnMouseEntered(mouseEvent -> {
            ring2.setFill(javafx.scene.paint.Color.ORANGE);
        });
        ring2.setOnMouseExited(mouseEvent -> {
            ring1.setFill(javafx.scene.paint.Color.BLACK);
            ring2.setFill(javafx.scene.paint.Color.BLACK);
            ring3.setFill(javafx.scene.paint.Color.BLACK);
        });

        ring3.setOnMouseEntered(mouseEvent -> {
            ring3.setFill(javafx.scene.paint.Color.GREEN);
        });
        ring3.setOnMouseExited(mouseEvent -> {
            ring1.setFill(javafx.scene.paint.Color.BLACK);
            ring2.setFill(javafx.scene.paint.Color.BLACK);
            ring3.setFill(javafx.scene.paint.Color.BLACK);
        });

    }


    public static void main(String[] args) {
        launch(args);
    }
}
